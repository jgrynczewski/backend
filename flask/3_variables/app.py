import random

from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/")
def index():
    headline = "Hello, world!"
    return render_template("index.html", context=headline)

@app.route("/bye")
def bye():
    headline = "Goodbye"
    return render_template("index.html", context=headline)

@app.route("/choice")
def my_function():
    headline = random.choice(["Hello, world!", "Goodbye", "Hi!", "Good morning"])
    return render_template("index.html", context=headline)
