from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy

from models import Task

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///notes.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy()
db.init_app(app)

@app.route("/", methods = ["GET", "POST"])
def index():
    if request.method == "POST":
        content = request.form.get("task")
        if content:
            t = Task(text=content)
            db.session.add(t)
            db.session.commit()

    tasks = Task.query.all()
    return render_template("index.html", context=tasks)