from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello, world!"

@app.route("/adam")
def adam():
    return "Hello, Adam!"

@app.route("/test")
def test():
    return "<html><head></head><body><h1>Hello, world!</h1></body></html>"

@app.route("/<string:name>")
def hello(name):
    return "Hello, {}!".format(name.title())

