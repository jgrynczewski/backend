from flask import Flask, render_template, request

app = Flask(__name__)

tasks = []

@app.route("/", methods = ["GET", "POST"])
def index():
    if request.method == "POST":
        task = request.form.get("task")
        tasks.append(task)
        print(tasks)
    return render_template("index.html", context=tasks)