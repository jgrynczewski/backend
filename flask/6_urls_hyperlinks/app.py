from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index_widok():
    return render_template("index.html")

@app.route("/sec")
def drugi_widok():
    return render_template("drugi.html")