import requests

base = input("Pierwsza waluta:")
other = input("Druga waluta:")
res = requests.get("http://data.fixer.io/api/latest?access_key=032053b70cf616de08638aeaeb1cfd1d",
                   params={"base": base, "symbols": other})
if res.status_code != 200:
    raise Exception("Error: API request unsuccessful.")
data = res.json()

rate = data["rates"][other]

print(f"1 {base} jest równoważny {rate} {other}")
