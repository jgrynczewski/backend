from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

text = [
    "To jest moja pierwsza strona",
    "To jest moja druga strona",
    "To jest moja trzeci strona"
]

@app.route("/first")
def first():
    return text[0]

@app.route("/second")
def second():
    return text[1]

@app.route("/third")
def third():
    return text[2]