import requests

from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/convert", methods=["POST"])
def convert():
    base = request.form.get("base")
    symbols = request.form.get("symbols")
    print(base)
    print(symbols)
    res = requests.get("http://data.fixer.io/api/latest?access_key=032053b70cf616de08638aeaeb1cfd1d",
                       params={"base": base, "symbols": symbols})

    if res.status_code != 200:
        return f"Niepowodzenie. Kod HTTP {res.status_code}"

    data = res.json()
    if not data.get("success"):
        return f"Coś poszło nie tak {data.get('error').get('type')}"
    return f"1 {base} to {data.get('rates').get(symbols)} {symbols}"