document.addEventListener('DOMContentLoaded', () => {

    document.querySelector("#form").onsubmit = () => {

        const request = new XMLHttpRequest();
        request.open('POST', '/convert');
        request.onload = () => {
            const data = request.responseText;

            document.querySelector("#result").innerHTML = data;
        }

        const base = document.querySelector("#base").value;
        const symbols = document.querySelector("#symbols").value;
        const data = new FormData();
        data.append('base', base);
        data.append('symbols', symbols);

        request.send(data);
        return false;

    }
});